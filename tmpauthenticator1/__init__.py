import uuid
import random
import logging

from traitlets import Bool
from tornado import gen

from jupyterhub.auth import Authenticator
from jupyterhub.handlers import BaseHandler
from jupyterhub.utils import url_path_join


class TmpAuthenticateHandler1(BaseHandler):
    """
    Handler for /tmplogin

    Creates a new user with a random UUID, and auto starts their server
    """
    def initialize(self, force_new_server, process_user):
        log = logging.getLogger("my-logger")
        log.error("q1-initialize")
        #self.redirect(url_path_join('http://',self.request.host,'/hub/spawn/user-redirect/lab/tree/workspace/AFIT%20Short%20Course%20-%20QAOA.ipynb'))
        super().initialize()
        self.force_new_server = force_new_server
        self.process_user = process_user

    @gen.coroutine
    def get(self):
        log = logging.getLogger("my-logger")
        log.error("q1-get")
        raw_user = yield self.get_current_user()
        if raw_user:
            if self.force_new_server and user.running:
                # Stop user's current server if it is running
                # so we get a new one.
                status = yield raw_user.spawner.poll_and_notify()
                if status is None:
                    yield self.stop_single_user(raw_user)
        else:
            #username = str(uuid.uuid4())
            #url = request.GET.get('username')
            #username = 'a_'+str(random.randint(0,9))
            username = self.get_argument("username", None, True)
            #username = username+'@usra.edu'
            username = username.replace("@usra.edu","")
            raw_user = self.user_from_username(username)
            self.set_login_cookie(raw_user)
        user = yield gen.maybe_future(self.process_user(raw_user, self))
        self.redirect(self.get_next_url(user))
        #stage
        log = logging.getLogger("my-logger")
        log.error(self.get_next_url(user))
        log.error(self.hub.base_url)
        log.error(self.request.full_url)
        log.error(self.request.host)
        log.error(self.get_next_url(user))
        #self.redirect(url_path_join('http://',self.request.host,'/hub/spawn/user-redirect/lab/tree/workspace/AFIT%20Short%20Course%20-%20QAOA.ipynb'))
        #self.redirect(url_path_join(self.settings.get('base_url'),'user-redirect/lab/tree/workspace/AFIT%20Short%20Course%20-%20QAOA.ipynb'))
        #prod
        #self.redirect('http://35.175.103.227:8000/hub/user-redirect/lab/tree/workspace/AFIT%20Short%20Course%20-%20QAOA.ipynb')
        #web_app = nb_server_app.web_app
        #logging.error('**quantum-error')
        #logging.info("**quantum-info")
        #logging.warning("**quantum-warning")
        #self.redirect(self.hub.base_url.'user-redirect/lab/tree/workspace/AFIT%20Short%20Course%20-%20QAOA.ipynb')
        #self.redirect(url_path_join(self.settings.get('base_url'),'user-redirect/lab/tree/workspace/AFIT%20Short%20Course%20-%20QAOA.ipynb'))
        #logging.error(Spawner.server.url)
        #self.get_argument("base_url")
        #logging.error(url_path_join(web_app.settings['base_url'],'user-redirect/lab/tree/workspace/AFIT%20Short%20Course%20-%20QAOA.ipynb'))
        #self.redirect(url_path_join(base_url, 'user/', username, '/lab'))
        #f = open("test.txt", "a")
        #f.write("test")
        #f.close()
        #self.redirect(self.get_next_url(user))


class TmpAuthenticator1(Authenticator):
    """
    JupyterHub Authenticator for use with tmpnb.org

    When JupyterHub is configured to use this authenticator, visiting the home
    page immediately logs the user in with a randomly generated UUID if they
    are already not logged in, and spawns a server for them.
    """
    log = logging.getLogger("my-logger")
    log.error("q1-TmpAuthenticator1")
    #self.redirect(url_path_join('http://3.84.225.37:8000/hub/spawn/user-redirect/lab/tree/workspace/AFIT%20Short%20Course%20-%20QAOA.ipynb'))
    auto_login = True
    login_service = 'tmp'

    force_new_server = Bool(
        False,
        help="""
        Stop the user's server and start a new one when visiting /hub/tmplogin

        When set to True, users going to /hub/tmplogin will *always* get a
        new single-user server. When set to False, they'll be
        redirected to their current session if one exists.
        """,
        config=True
    )

    def process_user(self, user, handler):
        """
        Do additional arbitrary things to the created user before spawn.

        user is a user object, and handler is a TmpAuthenticateHandler object

        Should return the new user object.

        This method can be a @tornado.gen.coroutine.

        Note: This is primarily for overriding in subclasses
        """
        log = logging.getLogger("my-logger")
        log.error("q1-process_user")
        #self.redirect(url_path_join('http://3.84.225.37:8000/hub/spawn/user-redirect/lab/tree/workspace/AFIT%20Short%20Course%20-%20QAOA.ipynb'))
        return user

    def get_handlers(self, app):
        # FIXME: How to do this better?
        log = logging.getLogger("my-logger")
        log.error("q1-get_handlers")
        extra_settings = {
            'force_new_server': self.force_new_server,
            'process_user': self.process_user
        }
        return [
            ('/tmplogin', TmpAuthenticateHandler1, extra_settings)
        ]

    def login_url(self, base_url):
        log = logging.getLogger("my-logger")
        log.error("q1-login_url")
        return url_path_join(base_url, 'tmplogin')
        #return 'http://3.84.225.37:8000/hub/spawn/user-redirect/lab/tree/workspace/AFIT%20Short%20Course%20-%20QAOA.ipynb'
