from setuptools import setup, find_packages

setup(
    name='jupyterhub-tmpauthenticator1',
    version='0.5',
    description='JupyterHub authenticator that hands out temporary accounts for everyone',
    url='',
    author='',
    author_email='',
    license='3 Clause BSD',
    packages=find_packages()
)
